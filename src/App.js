import React from 'react';
import ReactDOMServer from 'react-dom/server';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Jumbotron from "react-bootstrap/Jumbotron";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";


import data from './cscbase.json'; // forward slashes will depend on the file location

function App() {

var url_string = window.location.href
var url = new URL(url_string);
var idParam = url.searchParams.get("id");

//console.log(idParam);

//console.dir(data);

var elements = document.getElementsByTagName('NavDropdown');
//console.dir(elements);


var elemJSON = {
                "Analyst":[
                  {"title":"Basic Networking"},
                  {"title":"OSINT"},
                  {"title":"Security Technology Overview"},
                  {"title":"Introduction to Event Logging"}
                ]
                }

//console.dir(elemJSON);
//console.dir(elemJSON.Analyst);


//console.dir(elemJSON);
//elemJSON.Analyst[0].id = "test";
//console.log(elemJSON.Analyst[0].id);
//var counter = 0;

var handbookContent = "home";

for (var i=0;i<data.length;i++) {

  if ((data[i].link).length > 0) {
      //console.log(data[i].link);
      //counter++;
      //console.log(counter);
  }


}


if (idParam && idParam.length<4) {
      //  $(data[idParam].contenthtml).appendTo("#content");
//console.log(data[idParam].contenthtml);
//document.getElementById("content").innerHtml('test');

handbookContent = data[idParam].contenthtml;


}



if (idParam) {
  return (
    <Container>
    <NavBlock/>
    <Row>
      <Col id="content">{ReactHtmlParser(handbookContent)}</Col>
    </Row>
  </Container>
  );
} else {
  return (
    <Container style={{fontSize: 14 + 'px'}}>
    <NavBlock/>
    <ContentPage/>
  </Container>
  );
}


}

class NavBlock extends React.Component {
  render() {
      return (
        <Navbar bg="light" expand="lg">
          <Navbar.Brand href="/">CSC Handbook</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">


            <NavDropdown title="Overview" id="basic-nav-dropdown">
              <NavDropdown.Item href={updateUrl('What is a CTF')}>What is a CTF</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Beginners Guide to Cyber, Skills challenge and the CS Handbook')}>Beginners Guide to Cyber, Skills challenge and the CS Handbook</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Challenge Categories')}>Challenge Categories</NavDropdown.Item>
            </NavDropdown>



            <NavDropdown title="Analyst" id="basic-nav-dropdown">
              <NavDropdown.Item href={updateUrl('Analyst')}>Analyst Overview</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Basic Networking')}>Basic Networking</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('OSINT')}>OSINT</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Security Technology Overview')}>Security Technology Overview</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Introduction to Event Logging')}>Introduction to Event Logging</NavDropdown.Item>
            </NavDropdown>

            <NavDropdown title="Hardware" id="basic-nav-dropdown">
              <NavDropdown.Item href={updateUrl('Hardware')}>Hardware Overview</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Wifi')}>Wifi</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Arduino')}>Arduino</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Raspberry Pi')}>Raspberry Pi​</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Penetration Test Equipment and Tools')}>Penetration Test Equipment and Tools</NavDropdown.Item>
            </NavDropdown>

            <NavDropdown title="Cryptography" id="basic-nav-dropdown">
              <NavDropdown.Item href={updateUrl('Cryptography')}>Cryptography Overview</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Hash Algorithms')}>Hash Algorithms</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Encryption Overview')}>Encryption Overview</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Cryptoanalysis')}>Cryptoanalysis​</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Symmetric Encryption Algorithms')}>Symmetric Encryption Algorithms</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Asymmetric Encryption Algorithms')}>Asymmetric Encryption Algorithms</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Diffie-Hellman Key Agreement')}>Diffie-Hellman Key Agreement</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Digital Signatures')}>Digital Signatures</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('PKI')}>PKI</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Cipher Suite')}>Cipher Suite</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('NSA Suite B')}>NSA Suite B</NavDropdown.Item>
            </NavDropdown>


            <NavDropdown title="Programming" id="basic-nav-dropdown">
              <NavDropdown.Item href={updateUrl('Programming')}>Programming Overview</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Python')}>Python</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('HTML 5')}>HTML 5</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('C/C++')}>C/C++</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Assembly')}>Assembly</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Java')}>Java</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('JQuery')}>JQuery</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Web Dev - SASS')}>Web Dev - SASS</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Web Dev - Node.js')}>Web Dev - Node.js</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Web Dev - Grunt')}>Web Dev - Grunt</NavDropdown.Item>
            </NavDropdown>

            <NavDropdown title="Blue" id="basic-nav-dropdown">
              <NavDropdown.Item href={updateUrl('Forensics')}><strong>Forensics</strong></NavDropdown.Item>
                  <NavDropdown.Item href={updateUrl('Numbering systems and data types')}>Numbering systems and data types​​</NavDropdown.Item>
                  <NavDropdown.Item href={updateUrl('File constructs')}>File constructs</NavDropdown.Item>
                  <NavDropdown.Item href={updateUrl('Metadata')}>Metadata</NavDropdown.Item>
                  <NavDropdown.Item href={updateUrl('Disk structures')}>Disk structures</NavDropdown.Item>
                  <NavDropdown.Item href={updateUrl('File systems')}>File systems</NavDropdown.Item>
                  <NavDropdown.Item href={updateUrl('Forensic methodologies')}>Forensic methodologies</NavDropdown.Item>
                  <NavDropdown.Item href={updateUrl('Forensic tools')}>Forensic tools</NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Traffic Analysis')}><strong>Traffic Analysis</strong></NavDropdown.Item>
                  <NavDropdown.Item href={updateUrl('Packet Capture')}>Packet Capture</NavDropdown.Item>
                  <NavDropdown.Item href={updateUrl('PCAP Analysis')}>PCAP Analysis</NavDropdown.Item>
                  <NavDropdown.Item href={updateUrl('Network Traffic Analysis')}>Network Traffic Analysis</NavDropdown.Item>
            </NavDropdown>


            <NavDropdown title="Red" id="basic-nav-dropdown">
              <NavDropdown.Item href={updateUrl('Red')}><strong>Red Overview</strong></NavDropdown.Item>
              <NavDropdown.Item href={updateUrl('Discovery')}><strong>Discovery</strong></NavDropdown.Item>
                  <NavDropdown.Item href={updateUrl('Passive target enumeration')}>Passive target enumeration</NavDropdown.Item>
                  <NavDropdown.Item href={updateUrl('Active target enumeration')}>Active target enumeration</NavDropdown.Item>
            </NavDropdown>

            <NavDropdown title="Other" id="basic-nav-dropdown">
              <NavDropdown.Item href={updateUrl('Tools')}>​Tools</NavDropdown.Item>
            </NavDropdown>

            </Nav>
          </Navbar.Collapse>
        </Navbar>
      )
  }
}

class ContentPage extends React.Component {
    render() {
        return (
          <Container style={{marginTop: 40 + 'px'}}>
          <Jumbotron style={{backgroundColor: '#f4faff'}}>
          <h1>Cyber Skills Handbook</h1>

          <p>The Cyber Skills Challenge (CSC) community wiki.</p>



          </Jumbotron>

          <Row>




          <Col xs={8}>

          <h2>Contents</h2>

          <p>The CSC has several different categories it breaks its challenges down into categories to make it easier to practice and develop. The handbook covers:</p>


          <Row>
          <Col>

              <h4>Overview</h4>
              <ul>
                <li><a href={updateUrl('What is a CTF')}>What is a CTF</a></li>
                <li><a href={updateUrl('Beginners Guide to Cyber, Skills challenge and the CS Handbook')}>Beginners Guide to Cyber, Skills challenge and the CS Handbook</a></li>
                <li><a href={updateUrl('Challenge Categories')}>Challenge Categories</a></li>
              </ul>

              <a href={updateUrl('Analyst')}><h4>Analyst</h4></a>
              <ul>

                <li><a href={updateUrl('Basic Networking')}>Basic Networking</a></li>
                <li><a href={updateUrl('OSINT')}>OSINT</a></li>
                <li><a href={updateUrl('Security Technology Overview')}>Security Technology Overview</a></li>
                <li><a href={updateUrl('Introduction to Event Logging')}>Introduction to Event Logging</a></li>
              </ul>

              <a href={updateUrl('Hardware')}><h4>Hardware</h4></a>
              <ul>

                <li><a href={updateUrl('Wifi')}>Wifi</a></li>
                <li><a href={updateUrl('Arduino')}>Arduino</a></li>
                <li><a href={updateUrl('Raspberry Pi')}>Raspberry Pi​</a></li>
                <li><a href={updateUrl('Penetration Test Equipment and Tools')}>Penetration Test Equipment and Tools</a></li>
              </ul>

              <a href={updateUrl('Cryptography')}><h4>Cryptography</h4></a>
              <ul>

                <li><a href={updateUrl('Hash Algorithms')}>Hash Algorithms</a></li>
                <li><a href={updateUrl('Encryption Overview')}>Encryption Overview</a></li>
                <li><a href={updateUrl('Cryptoanalysis')}>Cryptoanalysis​</a></li>
                <li><a href={updateUrl('Symmetric Encryption Algorithms')}>Symmetric Encryption Algorithms</a></li>
                <li><a href={updateUrl('Asymmetric Encryption Algorithms')}>Asymmetric Encryption Algorithms</a></li>
                <li><a href={updateUrl('Diffie-Hellman Key Agreement')}>Diffie-Hellman Key Agreement</a></li>
                <li><a href={updateUrl('Digital Signatures')}>Digital Signatures</a></li>
                <li><a href={updateUrl('PKI')}>PKI</a></li>
                <li><a href={updateUrl('Cipher Suite')}>Cipher Suite</a></li>
                <li><a href={updateUrl('NSA Suite B')}>NSA Suite B</a></li>
              </ul>

              <a href={updateUrl('Programming')}><h4>Programming</h4></a>
              <ul>

                <li><a href={updateUrl('Python')}>Python</a></li>
                <li><a href={updateUrl('HTML 5')}>HTML 5</a></li>
                <li><a href={updateUrl('C/C++')}>C/C++</a></li>
                <li><a href={updateUrl('Assembly')}>Assembly</a></li>
                <li><a href={updateUrl('Java')}>Java</a></li>
                <li><a href={updateUrl('JQuery')}>JQuery</a></li>
                <li><a href={updateUrl('Web Dev - SASS')}>Web Dev - SASS</a></li>
                <li><a href={updateUrl('Web Dev - Node.js')}>Web Dev - Node.js</a></li>
                <li><a href={updateUrl('Web Dev - Grunt')}>Web Dev - Grunt</a></li>
              </ul>
</Col>
<Col>

              <h4>Blue</h4>
              <ul>


                <li><a href={updateUrl('Forensics')}><strong>Forensics</strong></a></li>
                    <li><a href={updateUrl('Numbering systems and data types')}>Numbering systems and data types​​</a></li>
                    <li><a href={updateUrl('File constructs')}>File constructs</a></li>
                    <li><a href={updateUrl('Metadata')}>Metadata</a></li>
                    <li><a href={updateUrl('Disk structures')}>Disk structures</a></li>
                    <li><a href={updateUrl('File systems')}>File systems</a></li>
                    <li><a href={updateUrl('Forensic methodologies')}>Forensic methodologies</a></li>
                    <li><a href={updateUrl('Forensic tools')}>Forensic tools</a></li>
                <li><a href={updateUrl('Traffic Analysis')}><strong>Traffic Analysis</strong></a></li>
                    <li><a href={updateUrl('Packet Capture')}>Packet Capture</a></li>
                    <li><a href={updateUrl('PCAP Analysis')}>PCAP Analysis</a></li>
                    <li><a href={updateUrl('Network Traffic Analysis')}>Network Traffic Analysis</a></li>
              </ul>


            <a href={updateUrl('Red')}><h4>Red</h4></a>
            <ul>

                <li><a href={updateUrl('Discovery')}><strong>Discovery</strong></a></li>
                    <li><a href={updateUrl('Passive target enumeration')}>Passive target enumeration</a></li>
                    <li><a href={updateUrl('Active target enumeration')}>Active target enumeration</a></li>
              </ul>


          <h4>Other</h4>
          <ul>
          <li><a href={updateUrl('Tools')}>​Tools</a></li>
          </ul>


</Col>
</Row>
</Col>

<Col xs={3}>


<h4>References:</h4>
<ul>
<li><a href="https://trailofbits.github.io/ctf/">​​​​​​CTF Field Guide</a></li>
</ul>

<h4>Practice Sites:</h4>
<ul>
<li><a href="https://www.hackthebox.eu/">​​​​Hack The Box</a></li>
<li><a href="https://www.vulnhub.com/">VulnHub​​</a></li>
<li><a href="http://overthewire.org/wargames/">OverTheWire</a></li>
</ul>

<h4>Resources:</h4>
<ul>
<li><a href="https://ctftime.org/event/list/upcoming">CTF Time​</a></li>
<li><a href="https://gitlab.com/cybears/fall-of-cybeartron">Cybears CTF: Fall of Cybeartron</a></li>
</ul>

<h4>Cheat Sheets:</h4>
<ul>
<li><a href="https://highon.coffee/blog/penetration-testing-tools-cheat-sheet/">Penetration Testing Tools Cheat Sheet</a></li>
<li><a href="http://pentestmonkey.net/cheat-sheet">Pentest Monkey</a></li>
<li><a href="https://www.corelan.be/index.php/2009/07/19/exploit-writing-tutorial-part-1-stack-based-overflows/">SSH & Meterpreter Pivoting Techniques</a></li>
<li><a href="https://blog.g0tmi1k.com/2011/08/basic-linux-privilege-escalation/">Basic Linux Privilege Escalation</a></li>
<li><a href="https://www.absolomb.com/2018-01-26-Windows-Privilege-Escalation-Guide/">Windows Privilege Escalation</a></li>
<li><a href="https://pequalsnp-team.github.io/cheatsheet/steganography-101">Steganography 101</a></li>
</ul>

<h4>Reading:</h4>
<ul>
<li><a href="https://en.wikipedia.org/wiki/Hacking:_The_Art_of_Exploitation">Hacking: The Art of Exploitation, Vol 2</a></li>
<li><a href="https://www.amazon.com/Hacker-Playbook-Practical-Penetration-Testing-ebook/dp/B01072WJZE">The Hacker Playbook 2: Practical Guide to Penetration Testing, Volume 2</a></li>
<li><a href="https://nostarch.com/ghpython.htm">Gray Hat Python</a></li>
</ul>

<h4>Guides:</h4>
<ul>
<li><a href="https://www.owasp.org/index.php/Top_10-2017_Top_10">​OWASP Top 10</a></li>
<li><a href="https://github.com/wwong99/pentest-notes/blob/master/oscp_resources/OSCP-Survival-Guide.md">OSCP Survival Guide</a></li>
<li><a href="https://www.corelan.be/index.php/2009/07/19/exploit-writing-tutorial-part-1-stack-based-overflows/">Exploit Writing Tutorial Part 1: Stack Based Overflows</a></li>
<li><a href="https://hackingandsecurity.blogspot.com/2017/10/a-red-teamers-guide-to-pivoting.html">A Red Teamer's Guide to Pivoting</a></li>
</ul>

<h4>Online Courses:</h4>
<ul>
<li><a href="https://www.offensive-security.com/information-security-training/penetration-testing-training-kali-linux/">Penetration Testing With Kali Linux (PWK)</a></li>
<li><a href="https://www.lynda.com/Security-tutorials/CompTIA-Security-SY0-501-Cert-Prep-1-Threats-Attacks-Vulnerabilities/599625-2.html">CompTIA Security+ (Lynda)</a></li>
<li><a href="https://stackskills.com/p/real-world-hacking-penetration-testing">Real World Hacking & Penetration Testing - StackSkills</a></li>
</ul>
</Col>
</Row>

</Container>

        )
    }
}


function updateUrl(param) {

var id = "";

  data.filter(function (i,n){

          //console.log(i.link,n);

          if (i.link == param) {
            //console.log(i.link);
            //console.log('item is: ' + n);
            id = n;
          }

      });

if (id) {
    return "?id="+id;
} else {
    console.log('No id match for: ' + param);
    return '#';

}


}

export default App;
